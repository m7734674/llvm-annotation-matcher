// @requires
// @func
int square(int num) {
    // @decl
    int out = 0;
    // @loop
    for (int i = 0; i < 10; i++) {
        // @add
        out += num * num;
    }
    // @ret
    return out;
}
