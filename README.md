LLVM-IR Annotation matcher
==========================
Very much proof of concept, there's not much here but it definitely seems feasible to match annotations in the source language to instructions in the LLVM-IR.

The `out.ll` file was generated using the command `clang -g -S -Xclang -disable-O0-optnone -emit-llvm in.c -o out.ll`.

# System requirements
- `llvm-17-dev`
- `libpolly-17-dev`


