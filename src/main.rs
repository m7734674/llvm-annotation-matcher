use llvm_ir::{HasDebugLoc,module::Module};
use std::fs;
use tree_sitter::{Parser, Query, QueryCursor};


fn main() -> Result<(), String> {
    let mut parser = Parser::new();
    parser.set_language(tree_sitter_c::language()).unwrap();
    let source = fs::read("in.c").unwrap();
    let tree = parser.parse(&source, None).unwrap();

    // let query = Query::new(tree_sitter_c::language(),r#"
    // (
    //     (comment)* @doc
    //     .
    //     [
    //         (function_definition
    //         declarator: [
    //             (identifier) @name
    //         ]) @definition.function
    //     ]
    //     (#strip! @doc "^//\\s*")
    //     (#select-adjacent! @doc @definition.function)
    // )
    // "#).unwrap();
    let query = Query::new(tree_sitter_c::language(),r#"
    (
        (comment)+ @doc
        .
        (_) @other
    )
    "#).unwrap();

    let mut cursor = QueryCursor::new();
    let mut comment_locs = Vec::new();

    for mat in cursor.matches(&query, tree.root_node(), source.as_slice()) {
        if mat.captures.last().map(|c|c.node.kind() != "comment").unwrap_or(false) {
            comment_locs.push((mat.captures[..mat.captures.len()-1].iter().map(|c|c.node.utf8_text(&source).unwrap()).collect::<String>(), mat.captures.last().unwrap().node.range(), false))
        }
    }

    let module = Module::from_ir_path("./out.ll")?;

    for function in module.functions {
        if let Some(func_loc) = function.get_debug_loc() {
            // println!("Func location: {:?}", func_loc);
            for (comment, range, found) in &mut comment_locs {
                if range.start_point.row == (func_loc.line - 1) as usize //&&
                    // if let Some(col) = func_loc.col {
                    //     range.start_point.column == (col - 1) as usize
                    // } else {
                    //     true
                    // }
                {
                    *found = true;
                    println!("{} matches {comment}", function.name);
                }
            }
        }

        for bb in &function.basic_blocks {
            for instruction in &bb.instrs {
                if let Some(instr_loc) = instruction.get_debug_loc() {
                    // println!("Instr location: {:?}", instr_loc);
                    for (comment, range, found) in &mut comment_locs {
                        if range.start_point.row == (instr_loc.line - 1) as usize //&&
                            // if let Some(col) = instr_loc.col {
                            //     range.start_point.column == (col - 1) as usize
                            // } else {
                            //     true
                            // }
                        {
                            *found = true;
                            println!("{instruction} matches {comment}");
                        }
                    }
                }
            }
            if let Some(term_loc) = bb.term.get_debug_loc() {
                // println!("Term location: {:?}", term_loc);
                for (comment, range, found) in &mut comment_locs {
                    if range.start_point.row == (term_loc.line - 1) as usize //&&
                        // if let Some(col) = term_loc.col {
                        //     range.start_point.column == (col - 1) as usize
                        // } else {
                        //     true
                        // }
                    {
                        *found = true;
                        println!("{} matches {comment}", bb.term);
                    }
                }
            }
        }
    }

    println!("\n\n");

    for (comment, range, found) in comment_locs {
        if !found{
            println!("Missing {comment} @ {range:?}");
        }
    }

    Ok(())
}
