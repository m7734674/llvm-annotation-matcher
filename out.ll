; ModuleID = 'in.c'
source_filename = "in.c"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

; Function Attrs: noinline nounwind uwtable
define dso_local i32 @square(i32 noundef %0) #0 !dbg !10 {
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  call void @llvm.dbg.declare(metadata i32* %2, metadata !15, metadata !DIExpression()), !dbg !16
  call void @llvm.dbg.declare(metadata i32* %3, metadata !17, metadata !DIExpression()), !dbg !18
  store i32 0, i32* %3, align 4, !dbg !18
  call void @llvm.dbg.declare(metadata i32* %4, metadata !19, metadata !DIExpression()), !dbg !21
  store i32 0, i32* %4, align 4, !dbg !21
  br label %5, !dbg !22

5:                                                ; preds = %14, %1
  %6 = load i32, i32* %4, align 4, !dbg !23
  %7 = icmp slt i32 %6, 10, !dbg !25
  br i1 %7, label %8, label %17, !dbg !26

8:                                                ; preds = %5
  %9 = load i32, i32* %2, align 4, !dbg !27
  %10 = load i32, i32* %2, align 4, !dbg !29
  %11 = mul nsw i32 %9, %10, !dbg !30
  %12 = load i32, i32* %3, align 4, !dbg !31
  %13 = add nsw i32 %12, %11, !dbg !31
  store i32 %13, i32* %3, align 4, !dbg !31
  br label %14, !dbg !32

14:                                               ; preds = %8
  %15 = load i32, i32* %4, align 4, !dbg !33
  %16 = add nsw i32 %15, 1, !dbg !33
  store i32 %16, i32* %4, align 4, !dbg !33
  br label %5, !dbg !34, !llvm.loop !35

17:                                               ; preds = %5
  %18 = load i32, i32* %3, align 4, !dbg !38
  ret i32 %18, !dbg !39
}

; Function Attrs: nofree nosync nounwind readnone speculatable willreturn
declare void @llvm.dbg.declare(metadata, metadata, metadata) #1

attributes #0 = { noinline nounwind uwtable "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #1 = { nofree nosync nounwind readnone speculatable willreturn }

!llvm.dbg.cu = !{!0}
!llvm.module.flags = !{!2, !3, !4, !5, !6, !7, !8}
!llvm.ident = !{!9}

!0 = distinct !DICompileUnit(language: DW_LANG_C99, file: !1, producer: "Debian clang version 14.0.6", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, splitDebugInlining: false, nameTableKind: None)
!1 = !DIFile(filename: "in.c", directory: "/home/alexander/llvm-annotation-matcher", checksumkind: CSK_MD5, checksum: "fe60f10bf166228663a92deda653556e")
!2 = !{i32 7, !"Dwarf Version", i32 5}
!3 = !{i32 2, !"Debug Info Version", i32 3}
!4 = !{i32 1, !"wchar_size", i32 4}
!5 = !{i32 7, !"PIC Level", i32 2}
!6 = !{i32 7, !"PIE Level", i32 2}
!7 = !{i32 7, !"uwtable", i32 1}
!8 = !{i32 7, !"frame-pointer", i32 2}
!9 = !{!"Debian clang version 14.0.6"}
!10 = distinct !DISubprogram(name: "square", scope: !1, file: !1, line: 3, type: !11, scopeLine: 3, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !0, retainedNodes: !14)
!11 = !DISubroutineType(types: !12)
!12 = !{!13, !13}
!13 = !DIBasicType(name: "int", size: 32, encoding: DW_ATE_signed)
!14 = !{}
!15 = !DILocalVariable(name: "num", arg: 1, scope: !10, file: !1, line: 3, type: !13)
!16 = !DILocation(line: 3, column: 16, scope: !10)
!17 = !DILocalVariable(name: "out", scope: !10, file: !1, line: 5, type: !13)
!18 = !DILocation(line: 5, column: 9, scope: !10)
!19 = !DILocalVariable(name: "i", scope: !20, file: !1, line: 7, type: !13)
!20 = distinct !DILexicalBlock(scope: !10, file: !1, line: 7, column: 5)
!21 = !DILocation(line: 7, column: 14, scope: !20)
!22 = !DILocation(line: 7, column: 10, scope: !20)
!23 = !DILocation(line: 7, column: 21, scope: !24)
!24 = distinct !DILexicalBlock(scope: !20, file: !1, line: 7, column: 5)
!25 = !DILocation(line: 7, column: 23, scope: !24)
!26 = !DILocation(line: 7, column: 5, scope: !20)
!27 = !DILocation(line: 9, column: 16, scope: !28)
!28 = distinct !DILexicalBlock(scope: !24, file: !1, line: 7, column: 34)
!29 = !DILocation(line: 9, column: 22, scope: !28)
!30 = !DILocation(line: 9, column: 20, scope: !28)
!31 = !DILocation(line: 9, column: 13, scope: !28)
!32 = !DILocation(line: 10, column: 5, scope: !28)
!33 = !DILocation(line: 7, column: 30, scope: !24)
!34 = !DILocation(line: 7, column: 5, scope: !24)
!35 = distinct !{!35, !26, !36, !37}
!36 = !DILocation(line: 10, column: 5, scope: !20)
!37 = !{!"llvm.loop.mustprogress"}
!38 = !DILocation(line: 12, column: 12, scope: !10)
!39 = !DILocation(line: 12, column: 5, scope: !10)
